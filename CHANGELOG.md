# Changelog

All notable changes to this project will be documented in this file.

## Release 0.7.0

**Features**

  * #44 IPv6 stack is enabled in the kernel but there is no interface that has ::1 address assigned
  * #46 drop puppet 4 add puppet 6

**Bugfixes**

  * #45 with new version of bundler it is possible bundle does not works
  * #41 template with hard coded username in helper for shell runner

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.6.3

**Features**

  * #35 the module ensure present pdk 1.7.1
  * #34 gitlab-runner needs to run docker with beaker

**Bugfixes**

  * #37 README update production ready
  * #36 the module is managed by pdk 1.7.1

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.6.2

**Features**

**Bugfixes**

  * #30 simplify `spec_helper_acceptance.rb`
  * #32 add tests about `manage_* => false`
  * #33 extend timeout to exec rbenv install

**Known Issues**

  * #2 Update configuration of an already configured runner.


## Release 0.6.1

**Features**

**Bugfixes**

  * #31 run tests with bundle

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.6.0

**Features**

**Bugfixes**

  * #28 install ruby with helper

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.5.2

**Features**

**Bugfixes**

  * #23 Enable tests with puppet 4 and puppet 5.
  * #24 Update module to build with pdk 1.7.0.
  * #25 Enable rubocop.

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.5.1

**Features**

  * #22 move to pdk 1.7.0.

**Bugfixes**

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.5.0

**Features**

**Bugfixes**

  * #9  README.md needs to be written.
  * #19 Add custom fact listing all runners and properties.
  * #20 Fix unregister runner.

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.4.0

**Features**

**Bugfixes**

  * #7  Fix unit tests (very first step).
  * #16 Fix name of variable `$::gitlabrunner::use_helper`.
  * #17 Use fact of release number instead of codename.

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.3.0

**Features**

**Bugfixes**

  * #6 Add `shell` executor.
  * #8 Fix custom type `Gitlabrunner::Settings`.

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.2.0

**Features**

**Bugfixes**

  * #4 Service resource needs package before.
  * #5 Option concurrent is a global option.

**Known Issues**
