type Gitlabrunner::Settings = Hash[
  Enum[
    'registration-token',
    'tags',
    'run-untagged',
    'url',
    'concurrent',
    'maximum-timeout',
    'limit',
    'executor',
    'executor-options',
  ],
  Variant[
    Boolean,
    Integer,
    String,
    Array,
    Hash,
  ]
]

