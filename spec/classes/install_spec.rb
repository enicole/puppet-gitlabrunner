require 'spec_helper'

describe 'gitlabrunner::install' do
  let(:pre_condition) do
    'include ::apt'
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
    end
  end
end
