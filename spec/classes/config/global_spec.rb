require 'spec_helper'

describe 'gitlabrunner::config::global' do
  let(:pre_condition) do
    needed_objects = <<-EOS
      require ::apt
      class { '::gitlabrunner::install' : }
    EOS
    needed_objects
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
    end
  end
end
