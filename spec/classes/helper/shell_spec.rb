require 'spec_helper'

gitlab_runner_user = 'gitlab-runner'
rbenv_root = "/home/#{gitlab_runner_user}/.rbenv"

describe 'gitlabrunner::helper::shell' do
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:pre_condition) do
        needed_objects = <<-EOS
          include apt
          include gitlabrunner::install
          include docker
        EOS
        needed_objects
      end
      let(:facts) { facts }

      context 'with manage_pdk => true' do
        let(:params) do
          {
            manage_docker: false,
            manage_pdk: true,
            manage_ruby: false,
            manage_vagrant: false,
            manage_virtualbox: false,
          }
        end

        case facts[:os]['release']['major']
        when '16.04'
          it { is_expected.to contain_package('pdk').with_ensure('1.9.1.0-1xenial') }
          it { is_expected.to compile }
        else
          it { is_expected.to compile.and_raise(Puppet::Error, %r{unmanaged distro}) }
        end
      end

      context 'with manage_vagrant => true' do
        let(:params) do
          {
            manage_docker: false,
            manage_pdk: false,
            manage_ruby: false,
            manage_vagrant: true,
            manage_virtualbox: false,
          }
        end

        case facts[:os]['release']['major']
        when '16.04'
          it { is_expected.to contain_archive('download vagrant package').with_ensure('present') }
          it { is_expected.to contain_exec('install vagrant').that_requires('Archive[download vagrant package]') }
          it { is_expected.to compile }
        else
          it { is_expected.to compile.and_raise(Puppet::Error, %r{unmanaged distro}) }
        end
      end

      context 'with manage_virtualbox => true' do
        let(:params) do
          {
            manage_docker: false,
            manage_pdk: false,
            manage_ruby: false,
            manage_vagrant: false,
            manage_virtualbox: true,
          }
        end

        case facts[:os]['release']['major']
        when '16.04'
          it { is_expected.to contain_class('virtualbox') }
          it { is_expected.to compile }
        else
          it { is_expected.to compile.and_raise(Puppet::Error, %r{unmanaged distro}) }
        end
      end

      context 'with manage_ruby => true' do
        let(:params) do
          {
            manage_docker: false,
            manage_pdk: false,
            manage_ruby: true,
            manage_vagrant: false,
            manage_virtualbox: false,
          }
        end

        case facts[:os]['release']['major']
        when '16.04'
          it { is_expected.to contain_package('libssl-dev') }
          it { is_expected.to contain_package('libreadline-dev') }
          it { is_expected.to contain_package('zlib1g-dev') }
          it { is_expected.to contain_file("#{rbenv_root}/plugins") }
          it { is_expected.to contain_file("#{rbenv_root}/cache") }
          it { is_expected.to contain_vcsrepo(rbenv_root.to_s) }
          it { is_expected.to contain_vcsrepo("#{rbenv_root}/plugins/ruby-build") }
          it { is_expected.to contain_exec('rbenv install 2.4.4') }
          it { is_expected.to contain_exec('rbenv install 2.5.1') }

          it { is_expected.to compile }
        else
          it { is_expected.to compile.and_raise(Puppet::Error, %r{unmanaged distro}) }
        end
      end

      context 'with manage_docker => true' do
        let(:params) do
          {
            manage_docker: true,
            manage_pdk: false,
            manage_ruby: false,
            manage_vagrant: false,
            manage_virtualbox: false,
          }
        end

        case facts[:os]['release']['major']
        when '16.04'
          it { is_expected.to contain_user(gitlab_runner_user.to_s).with_groups('docker') }

          it { is_expected.to compile }
        else
          it { is_expected.to compile.and_raise(Puppet::Error, %r{unmanaged distro}) }
        end
      end
    end
  end
end
