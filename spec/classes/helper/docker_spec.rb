require 'spec_helper'

describe 'gitlabrunner::helper::docker' do
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) { facts }

      context 'with manage_docker => false' do
        let(:params) do
          {
            manage_docker: false,
          }
        end

        it { is_expected.not_to contain_class('docker') }
        it { is_expected.to compile }
      end

      context 'with manage_docker => true' do
        let(:params) do
          {
            manage_docker: true,
          }
        end

        it { is_expected.to contain_class('docker') }
        it { is_expected.to compile }
      end
    end
  end
end
