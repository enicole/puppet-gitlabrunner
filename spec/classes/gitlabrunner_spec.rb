require 'spec_helper'

describe 'gitlabrunner' do
  let(:pre_condition) do
    'require ::apt'
  end

  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts.merge(
          'gitlabrunners' =>
          {
            'runnerdesc' =>
            {
              'executor'       => 'docker',
              'runner-token'   => 'azerty',
              'coordinatorurl' => 'https://coordinator.foo',
            },
          },
        )
      end

      it { is_expected.to compile }
    end
  end
end
