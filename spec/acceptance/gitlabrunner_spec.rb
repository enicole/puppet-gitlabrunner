require 'spec_helper_acceptance'

describe 'gitlabrunner class' do
  context 'with defaults parameters' do
    it 'applies idempotently' do
      pp = %(
        include apt
        include gitlabrunner
      )
      apply_manifest(pp, catch_failures: true)
      apply_manifest(pp, catch_changes: true)
    end

    # the package from gitlab.com should be used instead of the one provided by
    # the linux distrib sources. And so we have to set preferences
    describe file('/etc/apt/preferences.d/gitlab-runner.pref') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'root' }
      it { is_expected.to be_mode 644 }
      its(:content) { is_expected.to contain 'Package: gitlab-runner' }
      its(:content) { is_expected.to contain 'Pin: origin packages.gitlab.com' }
      its(:content) { is_expected.to contain 'Pin-Priority: 1001' }
    end

    # Gitlab provide an installer script that
    # install gitlab sources for package manager.
    describe file('/usr/local/sbin/gitlab_source_installer.sh') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'root' }
      it { is_expected.to be_mode 700 }
    end

    describe package('gitlab-runner') do
      it { is_expected.to be_installed }
    end

    describe file('/usr/lib/gitlab-runner/gitlab-runner') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'root' }
      it { is_expected.to be_mode 755 }
    end

    describe file('/usr/bin/gitlab-runner') do
      it { is_expected.to be_symlink }
      it { is_expected.to be_linked_to '../lib/gitlab-runner/gitlab-runner' }
    end

    describe file('/etc/gitlab-runner/config.toml') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'root' }
      it { is_expected.to be_mode 600 }
      its(:content) { is_expected.to contain 'concurrent = 4' }
    end

    describe service('gitlab-runner') do
      it { is_expected.to be_running }
    end

    describe package('docker-ce') do
      it { is_expected.not_to be_installed }
    end
    describe package('pdk') do
      it { is_expected.not_to be_installed }
    end
    describe package('vagrant') do
      it { is_expected.not_to be_installed }
    end
    describe package('libssl-dev') do
      it { is_expected.not_to be_installed }
    end
    describe package('libreadline-dev') do
      it { is_expected.not_to be_installed }
    end
    describe package('zlib1g-dev') do
      it { is_expected.not_to be_installed }
    end
    describe file('/home/gitlab-runner/.rbenv') do
      it { is_expected.not_to exist }
    end
  end

  context 'with executor => docker and manage_* to defaults' do
    it 'applies idempotently' do
      pp = %(
        include apt
        class { 'gitlabrunner':
          runners => {
            'puppet-gitlabrunner1-ci' => {
              registration-token => 'REGISTRATION_TOKEN',
              url                => 'https://gitlab.adullact.net',
              executor           => 'docker',
              executor-options   => {
                docker-image => 'ruby:3.0',
              },
            },
            'puppet-gitlabrunner2-ci' => {
              registration-token => 'REGISTRATION_TOKEN',
              url                => 'https://gitlab.adullact.net',
              executor           => 'docker',
              executor-options   => {
                docker-image => 'ruby:3.0',
              },
            },
          },
        }
      )
      apply_manifest(pp, catch_failures: true)
      apply_manifest(pp, catch_changes: true)
    end

    describe file('/etc/gitlab-runner/config.toml') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'root' }
      it { is_expected.to be_mode 600 }
      its(:content) { is_expected.to contain 'name = "puppet-gitlabrunner1-ci"' }
      its(:content) { is_expected.to contain 'name = "puppet-gitlabrunner2-ci"' }
    end

    describe command('gitlab-runner list') do
      its(:stderr) { is_expected.to match %r{.*puppet-gitlabrunner1-ci.*} }
      its(:stderr) { is_expected.to match %r{.*puppet-gitlabrunner2-ci.*} }
    end

    describe command('puppet facts | grep -A 2 gitlabrunners') do
      its(:stdout) { is_expected.to match %r{.*gitlabrunners.*} }
    end

    describe package('docker-ce') do
      it { is_expected.not_to be_installed }
    end
    describe package('pdk') do
      it { is_expected.not_to be_installed }
    end
    describe package('vagrant') do
      it { is_expected.not_to be_installed }
    end
    describe file('/home/gitlab-runner/.rbenv') do
      it { is_expected.not_to exist }
    end
  end

  context 'without runner defined and manage_* => true' do
    it 'applies unregister idempotently' do
      pp = %(
        include ::apt
        class { 'gitlabrunner' :
          manage_docker     => true,
          manage_pdk        => true,
          manage_ruby       => true,
          manage_vagrant    => true,
          manage_virtualbox => true,
        }
      )
      apply_manifest(pp, catch_failures: true)
      apply_manifest(pp, catch_changes: true)
    end

    describe file('/etc/gitlab-runner/config.toml') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'root' }
      it { is_expected.to be_mode 600 }
      its(:content) { is_expected.not_to contain 'name = "puppet-gitlabrunner1-ci"' }
      its(:content) { is_expected.not_to contain 'name = "puppet-gitlabrunner2-ci"' }
    end

    describe command('gitlab-runner list') do
      its(:stderr) { is_expected.not_to match %r{.*puppet-gitlabrunner1-ci.*} }
      its(:stderr) { is_expected.not_to match %r{.*puppet-gitlabrunner2-ci.*} }
    end

    describe package('docker-ce') do
      it { is_expected.to be_installed }
    end
    describe user('gitlab-runner') do
      it { is_expected.to belong_to_group 'docker' }
    end
    describe package('pdk') do
      it { is_expected.to be_installed }
    end
    describe package('vagrant') do
      it { is_expected.to be_installed }
    end
    describe package('libssl-dev') do
      it { is_expected.to be_installed }
    end
    describe package('libreadline-dev') do
      it { is_expected.to be_installed }
    end
    describe package('zlib1g-dev') do
      it { is_expected.to be_installed }
    end
    describe file('/home/gitlab-runner/.rbenv/plugins') do
      it { is_expected.to be_directory }
      it { is_expected.to be_owned_by 'gitlab-runner' }
      it { is_expected.to be_grouped_into 'gitlab-runner' }
    end
    describe file('/home/gitlab-runner/.profile') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'gitlab-runner' }
      it { is_expected.to be_grouped_into 'gitlab-runner' }
      it { is_expected.to be_mode 644 }
      its(:content) { is_expected.to match %r{PATH=/home/gitlab-runner/\.rbenv/bin:/home/gitlab-runner/\.rbenv/shims:\$PATH} }
    end
    describe file('/home/gitlab-runner/.rbenv/libexec/rbenv') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'gitlab-runner' }
      it { is_expected.to be_grouped_into 'gitlab-runner' }
    end
    describe command('su - gitlab-runner -c "rbenv whence ruby"') do
      its(:stdout) { is_expected.to match %r{.*2\.4\.4.*} }
      its(:stdout) { is_expected.to match %r{.*2\.5\.1.*} }
    end
    describe file('/home/gitlab-runner/.rbenv/versions/2.4.4/bin/bundle') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'gitlab-runner' }
      it { is_expected.to be_grouped_into 'gitlab-runner' }
    end
    describe file('/home/gitlab-runner/.rbenv/versions/2.5.1/bin/bundle') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'gitlab-runner' }
      it { is_expected.to be_grouped_into 'gitlab-runner' }
    end
    describe command('su - gitlab-runner -c "RBENV_VERSION=2.5.1 bundle --version"') do
      its(:stdout) { is_expected.to match %r{.*Bundler version.*} }
      its(:exit_status) { is_expected.to be == 0 }
    end
  end
end
