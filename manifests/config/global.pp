# @summary Modify global section in runner's configuration file /etc/gitlab-runner/config.toml
#
# @param concurrent
#  Limits how many jobs globally can be run concurrently. The most upper limit of jobs using all defined runners. 0 does not mean unlimited
class gitlabrunner::config::global (
  Integer $concurrent = 4,
) {
  $_config_file = '/etc/gitlab-runner/config.toml'

  file_line { 'concurrent' :
    ensure => present,
    path   => $_config_file,
    line   => "concurrent = ${concurrent}",
    match  => '^concurrent',
    notify => Service['gitlab-runner'],
  }
}
