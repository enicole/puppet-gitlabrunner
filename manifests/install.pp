# @summary Install gitlab runner.
#
# @example
#   include gitlabrunner::install
class gitlabrunner::install {

  $_required_packages = [
    'gnupg',
    'curl',
  ]
  $_package_name = 'gitlab-runner'
  $_package_version = '11.1.0'
  $_gitlab_source_installer_url = 'https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh'
  $_gitlab_source_installer_path = '/usr/local/sbin/gitlab_source_installer.sh'
  $_gitlab_source_installer_creates = '/etc/apt/sources.list.d/runner_gitlab-runner.list'

  package { $_required_packages :
    ensure => present,
  }

  archive { 'download gitlab source installer' :
    ensure => present,
    path   => $_gitlab_source_installer_path,
    source => $_gitlab_source_installer_url,
    user   => 0,
    group  => 0,
  }
  -> file { $_gitlab_source_installer_path :
    ensure => file,
    owner  => 0,
    group  => 0,
    mode   => '0700',
    notify => Exec[$_gitlab_source_installer_path],
  }

  exec { $_gitlab_source_installer_path :
    user        => 'root',
    refreshonly => true,
    require     => Package[$_required_packages],
    creates     => $_gitlab_source_installer_creates,
  }

  apt::pin { $_package_name :
    ensure      => present,
    explanation => 'Prefer GitLab provided packages over the Debian native one',
    packages    => $_package_name,
    origin      => 'packages.gitlab.com',
    priority    => 1001,
  }

  package { $_package_name :
    ensure  => $_package_version,
    require => [
      Apt::Pin[$_package_name],
      Exec[$_gitlab_source_installer_path],
    ],
  }

  service { 'gitlab-runner' :
    ensure  => running,
    start   => '/usr/bin/gitlab-runner start',
    stop    => '/usr/bin/gitlab-runner stop',
    status  => '/usr/bin/gitlab-runner status',
    require => Package[$_package_name],
  }

}
