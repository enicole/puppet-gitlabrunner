#@summary Install and configure gitlan ci runners.
#
#@example
#  class { 'gitlabrunner' :
#    concurrent    => 4,
#    manage_docker => true,
#    runners       => {
#      'runner01'           => {
#        registration-token => 'get.secret.from.gitlab.webui',
#        url                => 'https://mygitlabinstance.foo.org',
#        executor           => 'docker',
#        executor-options   => {
#          docker-image     => 'ruby:3.0',
#          docker-volumes   => '/var/run/docker.sock:/var/run/docker.sock',
#        }
#      }
#    }
#  }
#
#@param runners
#  List of runners with their settings. 
#  Managed options of runner are : 
#    * tags (Array used with --tag-list)
#    * maximum-timeout (Integer)
#    * limit (Integrer)
#    * run-untagged (Boolean)
#    * executor (Enum['docker','shell'])
#    * executor-options (Hash with any executor option, only docker for this module version)
#@param concurrent
#  Limits how many jobs globally can be run concurrently. The most upper limit of jobs using all defined runners. 0 does not mean unlimited
#@param manage_docker
#  This boolean defined if the module install Docker CE on runner box.
#@param manage_pdk
#  This boolean defined if the module install Puppet Development Kit on runner box (used with shell executor during puppet testing).
#@param manage_ruby
#  This boolean define if the module install Ruby 2.4.4 and 2.5.1 on the runner box (used with shell executor during puppet testing).
#@param manage_vagrant
#  This boolean define if the module install Vagrant 2.1.2 on the runner box (used with shell executor during puppet testing).
#@param manage_virtualbox
#  This boolean define if the module install Virtualbox 5.0 on the runner box (used with shell executor during puppet testing).
# 
class gitlabrunner (
  Hash[String,Hash] $runners = {},
  Integer $concurrent = 4,
  Boolean $manage_docker = false,
  Boolean $manage_pdk = false,
  Boolean $manage_ruby = false,
  Boolean $manage_vagrant = false,
  Boolean $manage_virtualbox = false,
) {

  include gitlabrunner::install

  $_defined_runners_in_facts = $facts['gitlabrunners'] ? {
    undef   => [],
    default =>  assert_type( Array, keys($facts['gitlabrunners'])) |$expected, $actual| { [] },
  }

  $_defined_runners_in_runners = $runners.map | $_items | { $_items[0] }
  $_defined_runners_in_facts.each | String $_runner_in_facts | {
    if !member($_defined_runners_in_runners , $_runner_in_facts) {
      gitlabrunner::unregister { $_runner_in_facts : }
    }
  }

  $runners.each | String $_runner_name, Gitlabrunner::Settings $_runner_settings | {

    $_runner_registration_token = assert_type(String[1], $_runner_settings['registration-token'])
    $_runner_coordinator_url = assert_type(Pattern['^https://'], $_runner_settings['url'])

    case $_runner_settings['executor'] {
      'docker','shell': {
        gitlabrunner::register { $_runner_name :
          registration_token => $_runner_registration_token,
          coordinator_url    => $_runner_coordinator_url,
          tags               => $_runner_settings['tags'],
          maximum_timeout    => $_runner_settings['maximum-timeout'],
          limit              => $_runner_settings['limit'],
          run_untagged       => $_runner_settings['run-untagged'],
          executor           => $_runner_settings['executor'],
          executor_options   => $_runner_settings['executor-options'],
          require            => Package[$::gitlabrunner::install::_package_name],
          notify             => Class['gitlabrunner::config::global'],
        }
      }
      default: {
        fail("executor ${_runner_settings['executor']} is not supported by this module")
      }
    }
  }

  class { 'gitlabrunner::config::global' :
    concurrent => $concurrent,
    require    => Package[$::gitlabrunner::install::_package_name],
  }
  class { 'gitlabrunner::helper::docker' :
    manage_docker => $manage_docker,
  }
  class { 'gitlabrunner::helper::shell' :
    manage_docker     => $manage_docker,
    manage_pdk        => $manage_pdk,
    manage_ruby       => $manage_ruby,
    manage_vagrant    => $manage_vagrant,
    manage_virtualbox => $manage_virtualbox,
  }

}
