# @summary Install Vagrant, Virtualbox, Puppet Developement Kit (aka pdk) used with shell executor for acceptance tests of Puppet modules.
#
#@param manage_docker
#  This boolean at true permits gitlab-runner to run docker commands (used with shell executor during puppet testing, hypervisor=docker).
#@param manage_pdk
#  This boolean at true the module install Puppet Development Kit on runner box (used with shell executor during puppet testing).
#@param manage_ruby
#  This boolean at true the module install Ruby 2.4.4 and 2.5.1 on the runner box (used with shell executor during puppet testing).
#@param manage_vagrant
#  This boolean at true the module install Vagrant 2.1.2 on the runner box (used with shell executor during puppet testing, hypervisor=vagrant).
#@param manage_virtualbox
#  This boolean at true the module install Virtualbox 5.0 on the runner box (used with shell executor during puppet testing).
#
class gitlabrunner::helper::shell (
  Boolean $manage_docker,
  Boolean $manage_pdk,
  Boolean $manage_ruby,
  Boolean $manage_vagrant,
  Boolean $manage_virtualbox,
) {

  $_runner_account = 'gitlab-runner'
  $_vagrant_package_name = 'vagrant'
  $_vagrant_package_version = '2.1.2'
  $_virtualbox_package_version = '5.2'
  $_pdk_package_name = 'pdk'
  $_pdk_package_version = '1.9.1.0'
  $_rbenv_root = "/home/${_runner_account}/.rbenv"
  $_rbenv_sources = 'https://github.com/rbenv/rbenv.git'
  $_rbenv_version = 'v1.1.1'
  $_rubybuild_sources = 'https://github.com/rbenv/ruby-build.git'
  $_rubybuild_version = 'v20180822'
  $_ruby_versions = ['2.4.4','2.5.1']

  case $facts['os']['release']['major'] {
    '16.04': {
      $_pdk_package_revision = '1xenial'
      $_pdk_real_package_version = "${_pdk_package_version}-${_pdk_package_revision}"
      $_pkg_update_cache = 'Exec[apt_update]'

      $_vagrant_package_url = "https://releases.hashicorp.com/vagrant/${_vagrant_package_version}/${_vagrant_package_name}_${_vagrant_package_version}_x86_64.deb"
      $_vagrant_package_path = "/root/${_vagrant_package_name}_${_vagrant_package_version}_x86_64.deb"
      $_vagrant_cmd_install = "/usr/bin/dpkg -i ${_vagrant_package_path}"
      $_vagrant_cmd_install_check = "/usr/bin/apt-cache show ${_vagrant_package_name} | grep '^Version:.*${_vagrant_package_version}$'"

      $_shell_custom_profile = "/home/${_runner_account}/.profile"
      $_ruby_requirement = [
        'libssl-dev',
        'libreadline-dev',
        'zlib1g-dev',
      ]
    }
    default: {
      fail("unmanaged os ${facts['os']['distro']['codename']}")
    }
  }

  if $manage_docker {
    user { $_runner_account :
      ensure  => present,
      groups  => 'docker',
      require => Class['docker'],
    }
  }

  if $manage_pdk {
    package { $_pdk_package_name :
      ensure  => $_pdk_real_package_version,
      require => $_pkg_update_cache,
    }
  }

  if $manage_vagrant {
    archive { 'download vagrant package' :
      ensure => present,
      path   => $_vagrant_package_path,
      source => $_vagrant_package_url,
      user   => 0,
      group  => 0,
    }
    -> exec { 'install vagrant' :
      command => $_vagrant_cmd_install,
      unless  => $_vagrant_cmd_install_check,
    }
  }

  if $manage_virtualbox {
    # install virtualbox
    class { 'virtualbox':
      version => $_virtualbox_package_version,
    }
  }

  if $manage_ruby {
    # install ruby
    package { $_ruby_requirement :
      ensure  => present,
      require => $_pkg_update_cache,
    }

    file_line { 'bash_profile_rbenv_path':
      path    => $_shell_custom_profile,
      line    => "PATH=/home/${_runner_account}/.rbenv/bin:/home/${_runner_account}/.rbenv/shims:\$PATH",
      require => Package['gitlab-runner'],
    }
    -> vcsrepo { $_rbenv_root :
      ensure   => present,
      provider => 'git',
      owner    => $_runner_account,
      group    => $_runner_account,
      source   => $_rbenv_sources,
      revision => $_rbenv_version,
    }
    -> file { ["${_rbenv_root}/plugins","${_rbenv_root}/cache"] :
      ensure => directory,
      owner  => $_runner_account,
      group  => $_runner_account,
      mode   => '0755',
    }
    -> vcsrepo { "${_rbenv_root}/plugins/ruby-build" :
      ensure   => present,
      provider => 'git',
      owner    => $_runner_account,
      group    => $_runner_account,
      source   => $_rubybuild_sources,
      revision => $_rubybuild_version,
    }

    $_ruby_versions.each | String $_ruby_version | {
      exec { "rbenv install ${_ruby_version}" :
        user        => $_runner_account,
        creates     => "${_rbenv_root}/versions/${_ruby_version}/bin/ruby",
        path        => "${_rbenv_root}/bin:${_rbenv_root}/shims:/bin:/usr/bin",
        cwd         => "/home/${_runner_account}",
        timeout     => 600,
        environment => [
          "HOME=/home/${_runner_account}",
        ],
        require     => [
          Package[$_ruby_requirement],
          Vcsrepo[$_rbenv_root],
          Vcsrepo["${_rbenv_root}/plugins/ruby-build"],
        ],
      }
      -> exec { "ruby ${_ruby_version} gem install bundler" :
        user        => $_runner_account,
        command     => 'gem install bundler',
        creates     => "${_rbenv_root}/versions/${_ruby_version}/bin/bundle",
        path        => "${_rbenv_root}/bin:${_rbenv_root}/shims:/bin:/usr/bin",
        cwd         => "/home/${_runner_account}",
        environment => [
          "HOME=/home/${_runner_account}",
          "RBENV_VERSION=${_ruby_version}",
        ],
      }
    }
  }
}
