# @summary Install Docker CE
#
#@param manage_docker
#  This boolean at true the module install Docker CE on runner box.
class gitlabrunner::helper::docker (
  Boolean $manage_docker,
) {

  if $manage_docker {
    # TODO : because docker is a moving product,
    # it is probably better to fix a value to docker version
    class { 'docker' :
      ipv6      => true,
      ipv6_cidr => '2001:db8:1::/64',
    }
  }
}
