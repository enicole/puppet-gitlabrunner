#
#@summary Register a runner to a gitlab-ci coordinator 
#
#@param registration_token
#  Runner's registration token
#@param executor
#  Select executor used by this runner.
#@param coordinator_url
#  Gitlab-ci coordinator url
#@param executor_options
#  List of options to use by the runner with its executor. List of available options : gitlab-runner register --help
#  With executor 'shell', only one option is accepted : 'bash' as String data type.
#@param tags
#  List of tags for this runner
#@param run_untagged
#  Permit to run untagged jobs with this runner
#@param maximum_timeout
#  What is the maximum timeout (in seconds) that will be set for job when using this Runner.
#@param limit
#   Maximum number of builds processed by this runner.
define gitlabrunner::register(
  String $registration_token,
  Enum['docker','shell'] $executor,
  Pattern['^https://'] $coordinator_url,
  Optional[Variant[Hash,String]] $executor_options = undef,
  Optional[Array] $tags = undef,
  Boolean $run_untagged = true,
  Integer $maximum_timeout = 0,
  Integer $limit = 0,
) {

  $_puppet_testing_tag = 'puppet-tests'
  $_runner_name = assert_type(String, $title)

  $_cmd_register_basis = "gitlab-runner register --non-interactive --executor ${executor} --name='${_runner_name}' --url '${coordinator_url}' --registration-token '${registration_token}' "

  if $tags {
    $_taglist = $tags.reduce | $_memo, $_tag | { "${_tag},${_memo}" }
    $_cmd_opt_tags = $executor ? {
      'shell' => "--tag-list ${_puppet_testing_tag},${_taglist}",
      default => "--tag-list ${_taglist}",
    }
  } else {
    $_cmd_opt_tags = $executor ? {
      'shell' => "--tag-list ${_puppet_testing_tag}",
      default => '',
    }
  }

  if $run_untagged {
    # in this module the shell executor is expected to run puppet acceptance tests
    # the shell runner should not accept jobs untagged (ie without tag $_puppet_acceptance_tag)
    $_cmd_opt_untagged = $executor ? {
      'shell' => '--run-untagged=false ',
      default => '--run-untagged=true ',
    }
  } elsif $run_untagged {
    # the shell executor does not use helper and so is generic shell runner
    # the shell runner should accept jobs untagged
    $_cmd_opt_untagged = '--run-untagged=true '
  } else {
    $_cmd_opt_untagged = '--run-untagged=false '
  }

  $_cmd_opt_maximum_timeout = "--maximum-timeout ${maximum_timeout}"
  $_cmd_opt_limit = "--limit ${limit}"

  if $executor_options {
    case $executor {
      'shell': {
        $valid_executor_options = assert_type(Enum['bash'], $executor_options)
        $_cmd_opts_executor = "--shell ${valid_executor_options}"
      }
      default: {
        $valid_executor_options = assert_type(Hash[Variant[Pattern['^docker-']], String], $executor_options)
        $_cmd_opts_executor = $valid_executor_options.reduce('') | $memo , $value | { "${memo} --${value[0]} ${value[1]}" }
      }
    }
  } else {
    $_cmd_opts_executor = ''
    # 
    # With docker executor, without executor options will cause the register command failure
    # with a message like 'The docker-image needs to be entered'. 
    # With docker executor, the option docker-image is mandatory. But we want to be able 
    # to know all the running images, only by reading hieradata.
    if $executor == 'docker' {
      fail ('With docker executor, the option docker-image is mandatory.')
    }
  }

  exec {"gitlab-runner-register-${_runner_name}":
    path    => '/usr/sbin:/usr/bin:/sbin:/bin',
    command => "${_cmd_register_basis} ${_cmd_opt_tags} ${_cmd_opt_untagged} ${_cmd_opt_maximum_timeout} ${_cmd_opt_limit} ${_cmd_opts_executor}",
    unless  => "gitlab-runner list 2> /dev/stdout | grep -qP '^${_runner_name}\s'",
  }

}
